from setuptools import find_packages, setup

setup(
    name='jeanne',
    version='0.0.1',
    description='A game not about Jeanne d’Arc',
    author='Lambert Family',
    url='https://framagit.org/Blaireau/jeanne',
    packages=find_packages(),
)
