import arcade



from jeanne.graphics.constants import TERRAIN_TEXTURE

class Application(arcade.Window):

    def __init__(self, controller, width, height, title):

        super().__init__(width, height, title, resizable=True, vsync=True)
        # self.set_exclusive_mouse(True)
        # self.set_mouse_visible(False)
        self.controller = controller
        self.terrain = arcade.Sprite(TERRAIN_TEXTURE)
        self.terrain.center_x = self.terrain.width / 2
        self.terrain.center_y = self.terrain.height / 2

    def exit(self):
        arcade.exit()

    def generate_terrain(self,terrainData):
        pass

    def on_mouse_press(self, *args, **kwargs):
        self.controller.on_mouse_press(*args, **kwargs)

    def on_mouse_release(self, *args, **kwargs):
        self.controller.on_mouse_release(*args, **kwargs)

    def on_mouse_motion(self, *args, **kwargs):
        self.controller.on_mouse_motion(*args, **kwargs)

    def on_mouse_scroll(self, *args, **kwargs):
        self.controller.on_mouse_scroll(*args, **kwargs)

    def on_key_press(self, *args, **kwargs):
        self.controller.on_key_press(*args, **kwargs)

    def on_resize(self, *args, **kwargs):
        self.controller.on_resize(*args, **kwargs)

    def on_draw(self):
        self.clear()
        self.terrain.draw()


