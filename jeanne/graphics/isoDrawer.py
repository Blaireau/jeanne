from pathlib import Path
import arcade
from jeanne.graphics.CONSTANTS import (
VERTICAL_DIAGONAL,
HORIZONTAL_DIAGONAL,
ORIGIN_X,
ORIGIN_Y)

from jeanne.graphics.filemanager import (
get_file_name)


class IsoDrawer:

    def __init__(self):
        self.terrainTiles = arcade.SpriteList()

    def generate_terrain(self, terrainData):
        #generate viewList and neighbors        
        
        for i in range(10):
            for j in range (5):
            
                image = arcade.Sprite(get_file_name(),
                                        scale = 1/4, 
                                        center_x = (j + 0.5 * (i%2)) * HORIZONTAL_DIAGONAL + ORIGIN_X, 
                                        center_y = (-i) * VERTICAL_DIAGONAL * 0.5 + ORIGIN_Y)
                
                self.terrainTiles.append(image)

    def draw(self):
        self.terrainTiles.draw()
