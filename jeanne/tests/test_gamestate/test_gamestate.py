from jeanne.gamestate.gamestate import GameState

import pytest

def test_switch_players():
    gs = GameState()
    red = gs.current_player
    blue = gs.other_player
    gs.switch_players()
    assert red is gs.other_player and blue is gs.current_player
