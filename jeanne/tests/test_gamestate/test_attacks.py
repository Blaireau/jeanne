from jeanne.gamestate.attacks import Attack, SimpleAttack, AttackEffect

def test_attack_effect():
    effect = AttackEffect(damage=5)
    assert effect.damage == 5

def test_attack_creation():
    attack = SimpleAttack(damage=2, distance=6)
    assert isinstance(attack, Attack)
    assert isinstance(attack.effect, AttackEffect)
    assert attack.effect.damage == 2
    assert attack.distance == 6
