from jeanne.gamestate.terrain import Terrain, Cell, TerrainType, NotOnTerrainError

import pytest

def coord_set(cellSet):
    return set((cell.x, cell.y) for cell in cellSet)

def compare_cells_sets(setA, setB):
    return coord_set(setA) == coord_set(setB)

def test_get_cells_circle_radius_0():
    terrain = Terrain.default()
    res = terrain.get_cells_circle(3, 5)
    expect = set([Cell(3, 5)])
    assert compare_cells_sets(res, expect)

def test_get_cells_circle_radius_1():
    terrain = Terrain.default()
    res = terrain.get_cells_circle(5, 5, radius=1)
    expect = set([Cell(5, 5), Cell(4, 5), Cell(6, 5), Cell(5, 4), Cell(5, 6)])
    assert compare_cells_sets(res, expect)

def test_get_cells_circle_radius_2_corner():
    terrain = Terrain.default()
    res = terrain.get_cells_circle(0, 0, radius=2)
    expect = set([Cell(0, 0), Cell(0, 1), Cell(0, 2), Cell(1, 0), Cell(1, 1), Cell(2, 0)])
    assert compare_cells_sets(res, expect)

def test_get_cells_circle_out_of_grid():
    with pytest.raises(NotOnTerrainError):
        terrain = Terrain.default()
        terrain.get_cells_circle(-2, 7)

def test_get_cells_move_no_obstacles():
    terrain = Terrain.from_string('''
____
____
____
____
''')
    res = terrain.get_cells_move(0, 0, radius=2, obstacles=TerrainType.MOUNTAIN)
    expect = set([Cell(0, 0), Cell(0, 1), Cell(0, 2), Cell(1, 0), Cell(1, 1), Cell(2, 0)])
    assert compare_cells_sets(res, expect)

def test_get_cells_move_simple():
    terrain = Terrain.from_string('''
_M__
_M_M
___M
~_M_
''')
    res = terrain.get_cells_move(2, 1, radius=5, obstacles=TerrainType.MOUNTAIN)
    expect = [Cell(0, 0), Cell(0, 1), Cell(0, 2), Cell(0, 3),
              Cell(1, 2), Cell(1, 3),
              Cell(2, 0), Cell(2, 1), Cell(2, 2),
              Cell(3, 0)]
    assert compare_cells_sets(res, expect)

def test_get_cells_move_blocked():
    terrain = Terrain.from_string('''
_____
__M__
_M_M_
__M__
_____
''')
    res = terrain.get_cells_move(2, 2, radius=1000000000, obstacles=TerrainType.MOUNTAIN)
    expect = set([Cell(2, 2)])
    assert compare_cells_sets(res, expect)

def test_get_cells_move_max_dist():
    terrain = Terrain.from_string('''
_____
MMMM_
_____
_MMMM
_____
''')
    res = terrain.get_cells_move(4, 4, radius=5, obstacles=TerrainType.MOUNTAIN)
    expect = set([Cell(4, 4), Cell(3, 4), Cell(2, 4), Cell(1, 4), Cell(0, 4), Cell(0, 3)])
    assert compare_cells_sets(res, expect)

def test_get_cells_move_out_of_grid():
    terrain = Terrain.from_string('''
MM___
__~__
_O__M
TT__Q
''')
    with pytest.raises(NotOnTerrainError):
        res = terrain.get_cells_move(5, 0, radius=8, obstacles=TerrainType.MOUNTAIN)

def test_get_cells_move_out_of_grid_2():
    terrain = Terrain.from_string('''
MM___
__~__
_O__M
TT__Q
''')
    with pytest.raises(NotOnTerrainError):
        res = terrain.get_cells_move(-5, 0, radius=8, obstacles=TerrainType.MOUNTAIN)

