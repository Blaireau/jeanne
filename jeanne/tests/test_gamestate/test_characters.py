from jeanne.gamestate.characters import Character
from jeanne.gamestate.attacks import SimpleAttack, AttackEffect


def test_character_creation():
    john = Character(life=30, speed=5, defaultAttack=None)
    assert john.life == 30
    assert john.speed == 5

def test_character_attack_interaction():
    effect = AttackEffect(damage=20)
    john = Character(life=30, speed=5, defaultAttack=None)
    assert john.life == 30
    assert john.isAlive
    john.apply_attack_effect(effect)
    assert john.life == 10
    assert john.isAlive
    john.apply_attack_effect(effect)
    assert john.life == 0
    assert not john.isAlive

def test_character_healing_interaction():
    effect = AttackEffect(damage=-5)
    john = Character(life=1, speed=5, defaultAttack=None)
    assert john.life == 1
    assert john.isAlive
    john.apply_attack_effect(effect)
    assert john.life == 6
    assert john.isAlive

def test_character_nothing_for_the_dead_():
    attack = AttackEffect(damage=5)
    heal = AttackEffect(damage=-5)
    john = Character(life=0, speed=5, defaultAttack=None)
    assert john.life == 0
    assert not john.isAlive
    john.apply_attack_effect(attack)
    assert not john.isAlive
    assert john.life == 0
    john.apply_attack_effect(heal)
    john.apply_attack_effect(heal)
    john.apply_attack_effect(heal)
    john.apply_attack_effect(heal)
    assert not john.isAlive
    assert john.life == 0
