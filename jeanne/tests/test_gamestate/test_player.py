from jeanne.gamestate.player import Player
from jeanne.gamestate.soldier import SOLDIER_PRICE
from jeanne.gamestate.errors import NotEnoughMoney

import pytest

def test_player_creation():
    player = Player(gold=50)
    assert player.gold == 50

def test_buy_soldier():
    initial_gold = 200
    player = Player(gold=initial_gold)
    res = player.buy_soldiers(quantity=2)
    assert len(res) == 2
    assert player.gold == initial_gold - SOLDIER_PRICE*2

def test_buy_not_enough_money():
    initial_gold = SOLDIER_PRICE - 1
    player = Player(gold=initial_gold)
    with pytest.raises(NotEnoughMoney):
        res = player.buy_soldiers(quantity=1)
    assert player.gold == initial_gold

def test_buy_negative_amount():
    player = Player(gold=500)
    with pytest.raises(ValueError):
        res = player.buy_soldiers(quantity=-1)
