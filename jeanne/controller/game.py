from jeanne.graphics.application import Application
from jeanne.gamestate.gamestate import GameState
from jeanne.controller.view_controller import ViewController
from jeanne.controller.settings import Settings


import PIL
from jeanne.graphics.constants import POS_FIELD_TEXTURE

class Game:

    def __init__(self):
        self.gamestate = GameState()
        windowSize = 800, 500
        self.app = Application(self, *windowSize, 'Not Jeanne d’Arc')
        self.settings = Settings()
        scrollBound = [0, 2000, 0, 2000]
        self.viewController = ViewController(windowSize, scrollBound)
        # use this to draw the terrain
        # see TerrainType in jeanne/gamestate/terrain
        terrainData = self.gamestate.get_terrain_data()

        self.pos_field = PIL.Image.open(POS_FIELD_TEXTURE)

        self.app.generate_terrain(terrainData)
        self.recompute_viewport()

    def on_key_press(self, key, modifiers):
        if key == self.settings.keys['exit']:
            self.app.exit()
        elif key == self.settings.keys['zoom in']:
            self.viewController.zoom_in()
            self.recompute_viewport()
        elif key == self.settings.keys['zoom out']:
            self.viewController.zoom_out()
            self.recompute_viewport()

    def to_terrain_cell(self, x_plane, y_plane):
        """ Get the cell coordinates from a position on the absolute plane"""
        inv_pos = x_plane, self.pos_field.height - y_plane
        try:
            px = self.pos_field.getpixel(inv_pos)
        except IndexError:
            return
        else:
            wpos = [round(v*51/256) for v in px[:3]]
            return wpos

    def on_mouse_press(self, x, y, button, modifiers):
        if button == self.settings.pan_vue_button:
            self.viewController.moving = True
        abs_pos = self.viewController.to_plane_pos(x, y)
        cell = self.to_terrain_cell(*abs_pos)
        print(cell)

    def on_mouse_release(self, x, y, button, modifiers):
        if button == self.settings.pan_vue_button:
            self.viewController.moving = False

    def on_mouse_motion(self, x, y, dx, dy):
        self.viewController.move_camera(dx, dy, self.settings.mouse_sensitivity)
        self.recompute_viewport()

    def on_mouse_scroll(self, x, y, scroll_x, scroll_y):
        if scroll_y > 0:
            self.viewController.zoom_in()
            self.recompute_viewport()
        elif scroll_y < 0:
            self.viewController.zoom_out()
            self.recompute_viewport()

    def on_resize(self, width, height):
        self.viewController.windowSize = width, height
        self.recompute_viewport()

    def recompute_viewport(self):
        viewport = self.viewController.viewport
        self.app.set_viewport(*viewport)

    def run(self):
        self.app.run()
