from jeanne.mathutils import constraint_in_range


class ViewController:

    def __init__(self, windowSize, boundingBox, zoomBound=(-1, 20)):
        self.windowSize = windowSize
        self.xmin, self.xmax, self.ymin, self.ymax = boundingBox
        self.camera_x = (self.xmin + self.xmax) / 2
        self.camera_y = (self.ymin + self.ymax) / 2

        self._zoom_level = 0 # a zoom level of 0 means the view matches the window size
                             # a bigger numbers zooms in
        self.zoomMin, self.zoomMax = zoomBound
        self.moving = False

    @property
    def viewport(self):
        dx = self.windowWidth * self.zoom / 2
        dy = self.windowHeight * self.zoom / 2
        xlow = self.camera_x - dx
        xhigh = xlow + 2*dx
        ylow = self.camera_y - dy
        yhigh = ylow + 2*dy
        return xlow, xhigh, ylow, yhigh

    @property
    def windowSize(self):
        return self.windowWidth, self.windowHeight

    @windowSize.setter
    def windowSize(self, windowSize):
        self.windowWidth, self.windowHeight = windowSize

    @property
    def zoom(self):
        return (7/8)**self._zoom_level

    def zoom_in(self):
        if self._zoom_level < self.zoomMax:
            self._zoom_level += 1

    def zoom_out(self):
        if self._zoom_level > self.zoomMin:
            self._zoom_level -= 1

    def move_camera(self, dx, dy, sensitivity):
        if self.moving:
            self.camera_x -= round(dx * self.zoom * sensitivity)
            self.camera_y -= round(dy * self.zoom * sensitivity)
            self._constraint_camera()

    def to_plane_pos(self, screen_x: int, screen_y: int) -> (int, int):
        x = self.camera_x + (screen_x - self.windowWidth / 2)*self.zoom
        y = self.camera_y + (screen_y - self.windowHeight / 2)*self.zoom
        return x, y

    def _constraint_camera(self):
        self.camera_x = constraint_in_range(self.xmin, self.xmax, self.camera_x)
        self.camera_y = constraint_in_range(self.ymin, self.ymax, self.camera_y)

