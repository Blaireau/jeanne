import arcade.key

class Settings:

    def __init__(self):
        self.keys = {'exit': arcade.key.Q,
                     'zoom in': arcade.key.PLUS,
                     'zoom out': arcade.key.MINUS
                    }
        self.mouse_sensitivity = 0.7
        self.pan_vue_button = arcade.MOUSE_BUTTON_MIDDLE
