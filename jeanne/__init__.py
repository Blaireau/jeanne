from pathlib import Path

ROOT = Path(__file__).resolve().parent.parent

ASSETS = ROOT / 'assets'
ASSETS_CACHE = ROOT / '__assetscache__'


