def constraint_in_range(minEdge: int, maxEdge: int, value: int) -> int:
    '''Return the nearest value that is still in the interval'''
    if value < minEdge:
        return minEdge
    if value > maxEdge:
        return maxEdge
    return value
