import cairosvg
from pathlib import Path
from jeanne import ASSETS, ASSETS_CACHE

# this is assuming the following structure:
# jeanne/
# |   assets/
# |   __assetscache__/
# |   jeanne/
# |   |   scripts/
# |   |      fileconverter.py

INPUT_FOLDER = ASSETS
OUTPUT_FOLDER = ASSETS_CACHE


def convert_all_assets(inputFolder, outputFolder):
    if not outputFolder.exists():
        outputFolder.mkdir()
    for file in inputFolder.iterdir():
        if file.is_dir():
            convert_all_assets(file, outputFolder / file.name)
        elif file.suffix == '.svg':
            outputFile = outputFolder / f'{file.stem}.png'
            cairosvg.svg2png(url=file.as_posix(), write_to=outputFile.as_posix())
        else:
            print(f'Warning: file {file} is not in svg format. I will ignore it.')

if __name__ == '__main__':
    convert_all_assets(INPUT_FOLDER, OUTPUT_FOLDER)

