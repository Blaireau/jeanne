from __future__ import annotations

from enum import Flag



class NotOnTerrainError(IndexError):
    pass

class TerrainType(Flag):
    '''Represent the type of terrain of a cell'''
    VOID = 0
    GRASS = 1
    RIVER = 2
    MOUNTAIN = 4
    FOREST = 8
    QUARRY = 16
    MINE = 32
    PIT = 64

    def match(self, other: TerrainType) -> TerrainType:
        return self & other

    @classmethod
    def from_string(cls, string: str) -> TerrainType:
        TYPES = {' ': cls.VOID,
                 '_': cls.GRASS,
                 'M': cls.MOUNTAIN,
                 '~': cls.RIVER,
                 'T': cls.FOREST,
                 'Q': cls.QUARRY,
                 'I': cls.MINE,
                 'O': cls.PIT}
        return TYPES[string]


class CellData:

    def __init__(self, terrainType: TerrainType):
        self.terrainType = terrainType


class TerrainData:
    '''Contains the type of terrain of each cell in the terrain
    Used to transmit terrain data to the graphics module'''

    def __init__(self, terrain: Terrain):
        self.grid = [[CellData(cell) for cell in column] for column in terrain.grid]
        self.width = terrain.width
        self.height = terrain.height

    def __getitem__(self, index: int) -> CellData:
        return self.grid[index]

    def __setitem__(self, index: int, value: CellData):
        self.grid[index] = value

class Cell:

    def __init__(self, x: int, y: int, terrainType=TerrainType.VOID):
        self.terrainType = terrainType
        self.x = x
        self.y = y

    def __repr__(self) -> str:
        return f'Cell({self.x}, {self.y})'

class Terrain:

    def __init__(self, width: int, height: int):
        self.width = width
        self.height = height
        self.grid = [[Cell(x, y) for y in range(self.height)] for x in range(self.width)]

    @classmethod
    def default(cls) -> Terrain:
        return Terrain(21, 11)

    @classmethod
    def from_string(cls, description: str) -> Terrain:
        lines = description.strip('\n').split('\n')
        height = len(lines)
        width = max([len(line) for line in lines])
        terrain = Terrain(width, height)
        for y, line in enumerate(lines):
            for x, elem in enumerate(line):
                terrain.grid[x][y].terrainType = TerrainType.from_string(elem)
        return terrain

    def generate(self):
        for cell in self.get_cells_circle(9, 4, radius=6):
            cell.terrainType = TerrainType.GRASS
        for x in range(self.width):
            cell = self.grid[x][4]
            if cell.terrainType == TerrainType.GRASS:
                cell.terrainType = TerrainType.RIVER

    def get_data(self) -> TerrainData:
        return TerrainData(self)

    def get_cell(self, x: int, y: int) -> Cell:
        if self.is_on_grid(x, y):
            return self.grid[x][y]
        else:
            raise NotOnTerrainError(f"Cell {x}, {y} is not on the grid.")

    def is_on_grid(self, x: int, y: int) -> bool:
        return 0 <= x < self.width and 0 <= y < self.height


    def get_cells_circle(self, x_center: int, y_center: int, radius=0) -> list[Cell]:
        if self.is_on_grid(x_center, y_center):
            circle = set()
            for x in range(-radius, radius+1):
                h = radius-abs(x)
                for y in range(-h, h+1):
                    coord_cell = x_center+x, y_center+y
                    if self.is_on_grid(*coord_cell):
                        circle.add(self.get_cell(*coord_cell))
        else:
            raise NotOnTerrainError(f"Center {x_center}, {y_center} is not on the grid.")
        return circle

    def get_cells_move(self, x_center: int,
                             y_center: int,
                             radius=0,
                             obstacles=TerrainType.VOID) -> list[Cell]:
       cells_reachable = set()
       test = set([self.get_cell(x_center, y_center)])
       for i in range(radius+1):
            test = {cell for cell in test if not cell.terrainType.match(obstacles)}
            test -= cells_reachable
            cells_reachable.update(test)
            newTest = set()
            for cell in test:
                newTest.update(self.get_cells_circle(cell.x, cell.y , 1))
            test = set(newTest)
            if not test:
                break
       return cells_reachable

