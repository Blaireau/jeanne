from jeanne.gamestate.characters import Character


SOLDIER_LIFE = 5
SOLDIER_SPEED = 3
SOLDIER_PRICE = 20


class Soldier(Character):

    def __init__(self):
        super().__init__(life=SOLDIER_LIFE, speed=SOLDIER_SPEED)
