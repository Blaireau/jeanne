from jeanne.gamestate.terrain import Terrain, TerrainData
from jeanne.gamestate.player import Player

class GameState:

    def __init__(self):
        self.terrain = Terrain.default()
        self.current_player = Player()
        self.other_player = Player()

    def switch_players(self):
        """switch current_player and other_player"""
        self.current_player, self.other_player = self.other_player, self.current_player

    def get_terrain_data(self) -> TerrainData:
        return self.terrain.get_data()
