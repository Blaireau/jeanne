from jeanne.gamestate.soldier import Soldier, SOLDIER_PRICE
from jeanne.gamestate.errors import NotEnoughMoney


class Player:

    def __init__(self, gold=0):
        self.gold = gold
        self.soldiers = []

    def buy_soldiers(self, quantity: int) -> list[Soldier]:
        if quantity < 0:
            raise ValueError(f'Number of soldiers should not be negative.')
        price = SOLDIER_PRICE*quantity
        if price < self.gold:
            self.gold -= price
            return [Soldier() for i in range(quantity)]
        raise NotEnoughMoney(f'Cannot by {quantity} soldiers with {self.gold} gold.')
