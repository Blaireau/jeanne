class Character:

    def __init__(self, life, speed, defaultAttack=None):
        self.life = life
        self.speed = speed
        self.defaultAttack = defaultAttack

    def apply_attack_effect(self, effect):
        if self.isAlive:
            if self.life >= effect.damage:
               self.life -= effect.damage
            else:
                self.life = 0
    @property
    def isAlive(self):
        return self.life > 0
