class AttackEffect:

    def __init__(self, damage):
        self.damage = damage


class Attack:
    def __init__(self, effect):
        self.effect = effect

class SimpleAttack(Attack):

    def __init__(self, damage, distance):
        effect = AttackEffect(damage)
        super().__init__(effect)
        self.distance = distance

