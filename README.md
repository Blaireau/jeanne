# Jeanne

Un jeu pas du tout sur Jeanne d’Arc

# Install the package and dependencies (dev)

Do this the first time, after cloning the repo

```
./setup
```

# Run the game

Run this from the root directory of the repo
```
./run
```
